//  import Vue from 'vue'

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

const firebaseConfig = {
  apiKey: 'AIzaSyDbL0b5jkozSQc_gciDJ5p_YNX3hB1jW1g',
  authDomain: 'velerioirezervacijesistem.firebaseapp.com',
  projectId: 'velerioirezervacijesistem',
  storageBucket: 'velerioirezervacijesistem.appspot.com',
  messagingSenderId: '781221609366',
  appId: '1:781221609366:web:afc5ec91c686ba364f67b7',
  measurementId: 'G-QDL2QEHD8E'
}

firebase.initializeApp(firebaseConfig)

export default ({ Vue }) => {
  Vue.prototype.$auth = firebase.auth()
  Vue.prototype.$db = firebase.firestore()
  Vue.prototype.$storage = firebase.storage()
}
