const routes = [
    {
        path: '/',
        component: () => import('layouts/HomePageLayout.vue'),
        children: [
          { path: '', component: () => import('pages/VeleriOIRezervacijeSystem/HomeIndex.vue') }
        ]
    },
  {
    path: '/Login',
    component: () => import('layouts/LoginPageLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Login/LoginIndex.vue') }
    ]
  },

  {
    path: '/Administration',
    component: () =>
      import('layouts/VeleriOIRezervacijeSystemLayout.vue'),
    meta: { auth: true },
    children: [
      {
        path: '/Korisnik',
        meta: { auth: true },
        component: () => import('pages/VeleriOIRezervacijeSystem/KorisnikIndex.vue')
      },
      {
        path: '/Perilica',
        meta: { auth: true },
        component: () => import('pages/VeleriOIRezervacijeSystem/PerilicaIndex.vue')
      }
    ]
  },
  {
    path: '/User',
    component: () =>
      import('layouts/VeleriOIRezervacijeSystemLayoutUser.vue'),
    children: [
      {
        path: '/KorisnikUser',
        component: () => import('pages/VeleriOIRezervacijeSystem/KorisnikIndexUser.vue')
      }
    ]
  }

]

// Always leave this as last one,
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
