const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


var admin = require("firebase-admin");

var serviceAccount = require("./private_key_AAAreserved.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL:"https://velerioirezervacijesistem.firebaseio.com"
});
const db = admin.firestore();


app.get('/Perilica', (request, response) => {
    let res = []
    db.collection('Perilica')
    .get()
    .then((querySnapshot) => {
    querySnapshot.forEach((doc) => {
    let document = {
    id: doc.id,
    data: doc.data()
 }
 res.push(document)
 })
 return response.send(res)
 })
 .catch(function (error) {
 return response.send("Error getting documents: " + error)
 })
})

app.post('/Perilica', (request, response) => {
    if (Object.keys(request.body).length) {
    db.collection('Perilica').doc().set(request.body)
    .then(function () {
    return response.send("Document successfully written!")
    })
    .catch(function (error) {
    return response.send("Error writing document: " + error)
    })
    } else {
    return response.send("No post data for new document. A new document isnot created!")
    }
   })
app.put('/Perilica', (request, response) => {
    if (Object.keys(request.body).length) {
    if (typeof request.query.id !== 'undefined') {
    db.collection('Perilica').doc(request.query.id).set(request.body)
    .then(function () {
    return response.send("Document successfully written - updated!")
    })
    .catch(function (error) {
    return response.send("Error writing document: " + error)
    })
    } else {
    return response.send("A parameter id is not set. A document is not updated!")
}
} else {
return response.send("No post data for new document. A document is not updated!")
}
})
app.delete('/Perilica', (request, response) => {
    if (typeof request.query.id !== 'undefined') {
    db.collection('Perilica').doc(request.query.id).delete()
    .then(function () {
    return response.send("Document successfully deleted!")
    })
    .catch(function (error) {
    return response.send("Error removing document: " + error)
    })
    } else {
    return response.send("A parameter id is not set. A document is not deleted!")
    }
   })
   
   app.get('/Korisnik', (request, response) => {
    let res = []
    db.collection('Korisnik')
    .get()
    .then((querySnapshot) => {
    querySnapshot.forEach((doc) => {
    let document = {
    id: doc.id,
    data: doc.data()
 }
 res.push(document)
 })
 return response.send(res)
 })
 .catch(function (error) {
 return response.send("Error getting documents: " + error)
 })
})

app.post('/Korisnik', (request, response) => {
    if (Object.keys(request.body).length) {
    db.collection('Korisnik').doc().set(request.body)
    .then(function () {
    return response.send("Document successfully written!")
    })
    .catch(function (error) {
    return response.send("Error writing document: " + error)
    })
    } else {
    return response.send("No post data for new document. A new document isnot created!")
    }
   })
app.put('/Korisnik', (request, response) => {
    if (Object.keys(request.body).length) {
    if (typeof request.query.id !== 'undefined') {
    db.collection('Korisnik').doc(request.query.id).set(request.body)
    .then(function () {
    return response.send("Document successfully written - updated!")
    })
    .catch(function (error) {
    return response.send("Error writing document: " + error)
    })
    } else {
    return response.send("A parameter id is not set. A document is not updated!")
}
} else {
return response.send("No post data for new document. A document is not updated!")
}
})
app.delete('/Korisnik', (request, response) => {
    if (typeof request.query.id !== 'undefined') {
    db.collection('Korisnik').doc(request.query.id).delete()
    .then(function () {
    return response.send("Document successfully deleted!")
    })
    .catch(function (error) {
    return response.send("Error removing document: " + error)
    })
    } else {
    return response.send("A parameter id is not set. A document is not deleted!")
    }
   })
   
app.listen(3000, () => {
 console.log("Server running on port 3000");
});